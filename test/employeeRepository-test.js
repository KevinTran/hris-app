const mocha = require('mocha');

const models = require('../app/server/models');

const employeeRepo = require('../app/server/data/employeeRepository');

describe('employeeRepo', () => {
  let transaction;

  beforeEach((done) => {
    models.sequelize.transaction().then((t) => {
      transaction = t;
      done();
    });
  });

  afterEach((done) => {
    transaction.rollback();
    done();
  });

  it('Fetches ACTIVE employees', (done) => {
    employeeRepo.getEmployees({ status: 'ACTIVE' })
      .then(() => done())
      .catch(error => done(error));
  });

  it('Fetches TEMPORARY employees', (done) => {
    employeeRepo.getEmployees({ status: 'TEMPORARY' })
      .then(() => done())
      .catch(error => done(error));
  });

  it('Creates and updates a TEMPORARY employee', (done) => {
    employeeRepo.createEmployee({
      employeeId: -1,
      status: 'TEMPORARY',
      firstName: 'Test',
      lastName: 'Person',
      gender: 'M',
      dateOfBirth: new Date(),
      title: 'Tester',
      createDatetime: new Date(),
      createEmployee: 0
    }, transaction)
      .then((record) => {
        employeeRepo.updateEmployee(+record.dataValues.employeeId, {
          firstName: 'Test-2',
          updateDatetime: new Date(),
          updateEmployee: 0
        }, transaction)
          .then(() => done())
          .catch(error => done(error));
      })
      .catch(error => done(error));
  });
});