const mocha = require('mocha');

const models = require('../app/server/models');

const formRepo = require('../app/server/data/formRepository');

describe('formRepository', () => {
  let transaction;

  beforeEach((done) => {
    console.log('Initializing transaction');
    models.sequelize.transaction().then((t) => {
      transaction = t;
      done();
    });
  });

  afterEach((done) => {
    console.log('Rolling back transaction');
    transaction.rollback();
    done();
  });

  it('Fetches forms by employee ID', (done) => {
    formRepo.getByEmployeeId(0)
      .then(() => done())
      .catch(error => done(error));
  });

  it('Creates form', (done) => {
    formRepo.create({
      employeeId: 0,
      formName: 'EMPLOYMENT_HISTORY',
      formDisplay: 'Employment History',
      status: 'ACTIVE',
      createDatetime: new Date(),
      createEmployee: 0
    }, transaction)
      .then(record => done())
      .catch(error => done(error));
  });

  it('Updates form', (done) => {
    formRepo.update(0, {
      status: 'COMPLETE',
      updateDatetime: new Date(),
      updateEmployee: 0
    }, transaction)
      .then(records => done())
      .catch(error => done(error));
  });
});