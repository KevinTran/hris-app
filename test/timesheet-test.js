const mocha = require('mocha');
const moment = require('moment');

const models = require('../app/server/models');

const timesheetRepo = require('../app/server/data/timesheetRepository');

describe('timesheetRepo', () => {
  let transaction;

  beforeEach((done) => {
    models.sequelize.transaction().then((t) => {
      transaction = t;
      done();
    });
  });

  afterEach((done) => {
    transaction.rollback();
    done();
  });

  it('Create timesheet', (done) => {
    timesheetRepo.createTimesheet({
      timesheetId: -1,
      employeeId: 5,
      status: 'ACTIVE',
      startDate: moment(new Date('01-MAR-2018')).toDate(),
      endDate: moment(new Date('15-MAR-2018')).toDate(),
      createDatetime: new Date(),
      createEmployee: 0
    }, transaction)
      .then(() => done())
      .catch(error => done(error));
  });

  it('Update timesheet', (done) => {
    timesheetRepo.updateTimesheet(1, {
      status: 'APPROVED',
      approveDatetime: new Date(),
      approveEmployee: 0,
      updateDatetime: new Date(),
      updateEmployee: 0
    }, transaction)
      .then(() => done())
      .catch(error => done(error));
  });

  it('Fetches timesheet', (done) => {
    timesheetRepo.getTimesheet(1)
      .then(() => done())
      .catch(error => done(error));
  });

  it('Fetches current timesheet values by employee', (done) => {
    timesheetRepo.getCurrentTimesheetValuesByEmployee(0, new Date(), transaction)
      .then(() => done())
      .catch(error => done(error));
  });
});