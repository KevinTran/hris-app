const mocha = require('mocha');

const models = require('../app/server/models');

const accountRepo = require('../app/server/data/accountRepository');

const securityUtility = require('../app/server/utilities/securityUtility');

describe('accountRepo', () => {
  let transaction;

  beforeEach((done) => {
    console.log('Initializing transaction');
    models.sequelize.transaction().then((t) => {
      transaction = t;
      done();
    });
  });

  afterEach((done) => {
    console.log('Rolling back transaction');
    transaction.rollback();
    done();
  });

  it('Fetches account by username', (done) => {
    accountRepo.getAccountByUsername('admin')
      .then(() => done())
      .catch(error => done(error));
  });

  it('Creates account', (done) => {
    securityUtility.createOrUpdateAccount(1, 'test', 'helloworld', 0, transaction)
      .then(() => done())
      .catch(error => done(error));
  });

  it('Authenticates user', (done) => {
    securityUtility.authenticateUser('test', 'helloworld', transaction)
      .then(results => done())
      .catch(error => done(error));
  });
});