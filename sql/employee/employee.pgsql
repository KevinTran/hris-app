/******************************************************************************
 ** Description: SQL statements for employees
 ** @author Kevin Tran
 ******************************************************************************/

GRANT USAGE ON SCHEMA employee TO "appUser";

CREATE SEQUENCE employee.employee_seq
  START WITH 1
  MINVALUE 1
  MAXVALUE 100 NO CYCLE;

CREATE TABLE employee.employee (
  employee_id         numeric(6, 0) NOT NULL DEFAULT nextval('employee.employee_seq'::regclass),
  status              varchar(25) NOT NULL,
  first_name          varchar(25) NOT NULL,
  last_name           varchar(25) NOT NULL,
  gender              varchar(1) NOT NULL,
  date_of_birth       date NOT NULL,
  title               varchar(25) NOT NULL,
  pay_rate            numeric(4, 2) NOT NULL DEFAULT 0,
  manager_id          numeric(6, 0) NULL,
  hire_datetime       timestamp NULL,
  phone_number        varchar(10) NULL,
  create_datetime     timestamp NOT NULL,
  create_employee     numeric(6, 0) NOT NULL,
  inactivate_datetime timestamp NULL,
  inactivate_employee numeric(6, 0) NULL,
  update_datetime     timestamp NULL,
  update_employee     numeric(6, 0) NULL,
  PRIMARY KEY (employee_id)
);

GRANT USAGE ON employee.employee_seq TO "appUser";

GRANT SELECT, UPDATE, INSERT ON employee.employee TO "appUser";

CREATE TABLE employee.employee_address (
  employee_id     numeric(6, 0) NOT NULL,
  address         varchar(25) NOT NULL,
  city            varchar (25) NOT NULL,
  state           varchar(2) NOT NULL,
  zipcode         varchar(25) NOT NULL,
  create_datetime timestamp NOT NULL,
  create_employee numeric(6, 0) NOT NULL,
  update_datetime timestamp NULL,
  update_employee numeric(6, 0) NULL,
  PRIMARY KEY (employee_id)
);

GRANT SELECT, UPDATE, INSERT ON employee.employee_address TO "appUser";