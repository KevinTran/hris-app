/******************************************************************************
 ** Description: SQL statements for forms
 ** @author Kevin Tran
 ******************************************************************************/

CREATE SEQUENCE employee.form_seq
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	START WITH 1
	CACHE 1
	NO CYCLE;

CREATE TABLE "employee"."form"  (
	"form_id"        	numeric(10, 0) NOT NULL DEFAULT nextval('employee.form_seq'::regclass),
  "employee_id"    	numeric(6, 0) NOT NULL,
	"form_name"      	varchar(50) NOT NULL,
  "form_display"    varchar(50) NOT NULL,
	"form_data"      	json NULL,
  "status"          varchar(25) NOT NULL,
	"create_datetime"	timestamp NOT NULL,
	"create_employee"	numeric(6, 0) NOT NULL,
	"update_datetime"	timestamp NULL,
	"update_employee"	numeric(6, 0) NULL,
	PRIMARY KEY("form_id")
);

GRANT USAGE ON employee.form_seq TO "appUser";

GRANT SELECT, INSERT, UPDATE ON employee.form TO "appUser";