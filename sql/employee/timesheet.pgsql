/******************************************************************************
 ** Description: SQL statements for timesheets
 ** @author Kevin Tran
 ******************************************************************************/

/* ----- Timesheet --------------------------------------------------------- */
CREATE TABLE employee.timesheet (
  timesheet_id      numeric(6, 0) NOT NULL DEFAULT nextval('employee.timesheet_seq'::regclass),
  employee_id       numeric(6, 0) NOT NULL,
  status            varchar(25) NOT NULL,
  start_date        date NOT NULL,
  end_date          date NOT NULL,
  hours             numeric(4, 2) NULL,
  amount_paid       numeric(6, 2) NULL,
  create_datetime   timestamp NOT NULL,
  create_employee   numeric(6, 0) NOT NULL,
  update_datetime   timestamp NULL,
  update_employee   numeric(6, 0) NULL,
  PRIMARY KEY (timesheet_id),
  CONSTRAINT employee_ckey UNIQUE (
    employee_id,
    status,
    start_date,
    end_date
  );
);

GRANT SELECT, UPDATE, INSERT ON employee.timesheet TO "appUser";

CREATE SEQUENCE employee.timesheet_seq
  START WITH 1
  MINVALUE 1
  MAXVALUE 100 NO CYCLE;

GRANT SELECT, usage ON employee.timesheet_seq TO "appUser";

/* ----- Timesheet Value --------------------------------------------------- */
CREATE TABLE employee.timesheet_value (
  timesheet_value_id  numeric(10, 0) NOT NULL DEFAULT nextval('employee.timesheet_value_seq'::regclass),
  timesheet_id        numeric(10, 0) NOT NULL,
  timesheet_date      date NULL,
  time_in             timestamptz NULL,
  time_out            timestamptz NULL,
  hours               numeric(6, 2) NULL,
  create_datetime     timestamp NOT NULL,
  create_employee     numeric(6, 0) NOT NULL,
  update_datetime     timestamp NULL,
  update_employee     numeric(6, 0) NULL,
  PRIMARY KEY (timesheet_value_id)
);

GRANT SELECT, UPDATE, INSERT ON employee.timesheet_value TO "appUser";

CREATE SEQUENCE employee.timesheet_value_seq
  START WITH 1
  MINVALUE 1
  MAXVALUE 1000 NO CYCLE;

GRANT SELECT, usage ON employee.timesheet_value_seq TO "appUser";

/* ----- Build Timesheet Data ---------------------------------------------- */
CREATE OR REPLACE FUNCTION create_timesheet ()
  RETURNS TRIGGER
AS $ai_timesheet$
DECLARE
  r record;
BEGIN
    INSERT INTO employee.timesheet_value (timesheet_id, timesheet_date, create_datetime, create_employee)
    VALUES (NEW.timesheet_id, NEW.start_date, now(), 0);
  RETURN NEW;
END;
$ai_timesheet$
LANGUAGE plpgsql;

CREATE TRIGGER ai_timesheet AFTER INSERT
  ON employee.timesheet FOR EACH ROW EXECUTE PROCEDURE create_timesheet ();

/* ----- Calculate Hours Worked -------------------------------------------- */
CREATE OR REPLACE FUNCTION update_timesheet_value ()
  RETURNS TRIGGER
AS $bu_timesheet_value$
DECLARE
  r record;
  d interval;
BEGIN
  IF NEW.time_out IS NOT NULL THEN
    IF NEW.time_in IS NULL THEN
      RAISE
      EXCEPTION 'TIME_OUT REQUIRES TIME_IN VALUE';
    ELSIF NEW.time_out <= NEW.time_in THEN
      RAISE
      EXCEPTION 'TIME_OUT MUST BE GREATER THAN TIME_IN';
    END IF;
  END IF;
  d := NEW.time_out::time - NEW.time_in::time;
  NEW.hours := EXTRACT(HOUR FROM d) + EXTRACT(MINUTE FROM d) / 60 + EXTRACT(SECOND FROM d) / 60 / 60;

  UPDATE employee.timesheet
  SET hours = (SELECT SUM(v.hours) + NEW.hours FROM employee.timesheet_value v WHERE v.timesheet_id = NEW.timesheet_id)
  WHERE timesheet_id = NEW.timesheet_id;

  RETURN NEW;
END;
$bu_timesheet_value$
LANGUAGE plpgsql;

CREATE TRIGGER bu_timesheet_value BEFORE UPDATE
  ON employee.timesheet_value FOR EACH ROW EXECUTE PROCEDURE update_timesheet_value ();

CREATE OR REPLACE FUNCTION update_timesheet ()
  RETURNS TRIGGER
AS $bu_timesheet$
DECLARE
  r record;
  v_pay_rate numeric(4, 2);
BEGIN
  IF NEW.status = 'PAID' THEN
    SELECT pay_rate
    INTO v_pay_rate
    FROM employee.employee
    WHERE employee_id = NEW.employee_id;

    IF v_pay_rate IS NULL THEN
      v_pay_rate := 0;
    END IF;

    NEW.amount_paid := NEW.hours * v_pay_rate;
  END IF;

  RETURN NEW;
END;
$bu_timesheet$
LANGUAGE plpgsql;

CREATE TRIGGER bu_timesheet BEFORE UPDATE
  ON employee.timesheet FOR EACH ROW EXECUTE PROCEDURE update_timesheet ();