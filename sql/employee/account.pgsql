/******************************************************************************
 ** Description: SQL statements for user accounts
 ** @author Kevin Tran
 ******************************************************************************/

-- Create table to store user accounts
CREATE TABLE employee.account (
  employee_id   numeric(6, 0) NOT NULL,
  username      varchar(25) NOT NULL,
  salt          varchar(30) NOT NULL,
  password_hash varchar(60) NOT NULL,
  PRIMARY KEY (employee_id)
);
/

-- Give table access to "appUser"
GRANT SELECT, INSERT, UPDATE ON employee.account TO "appUser";
/

-- Install pgcrypto extension
CREATE EXTENSION pgcrypto;
/

-- Create stored procedure to create/update user accounts
CREATE OR REPLACE FUNCTION create_account(
  p_employee_id       numeric(6,0),
  p_username          varchar(25),
  p_password          varchar(25),
  p_create_employee   numeric(6,0)
)
RETURNS void AS $$
DECLARE
  v_can_edit        boolean := false;
  v_username_exists boolean := false;
  v_salt            varchar;
  v_hash            varchar;
  v_has_account     boolean := false;
BEGIN
  SELECT
    CASE
      WHEN (p_create_employee = -1 AND (SELECT COUNT(*) FROM employee.account) = 0)
        OR EXISTS (
          SELECT 1
          FROM employee.employee
          WHERE employee_id = p_create_employee
            AND title IN ('HR User', 'Manager')
        ) THEN 1
      ELSE 0
    END
  INTO v_can_edit;

  IF v_can_edit = false THEN
    RAISE EXCEPTION 'User does not have permission to create accounts';
  END IF;

  SELECT
    CASE
      WHEN EXISTS (
        SELECT 1
        FROM employee.account
        WHERE username = p_username
          AND employee_id != p_employee_id) THEN 1
      ELSE 0
    END
  INTO v_username_exists;

  IF v_username_exists THEN
    RAISE EXCEPTION 'Username already taken';
  END IF;

  v_salt := gen_salt('bf', 8);
  v_hash := crypt(p_password, v_salt);

  SELECT
    CASE WHEN EXISTS (SELECT 1 FROM employee.account WHERE employee_id = p_employee_id) THEN 1
      ELSE 0
    END
  INTO v_has_account;

  IF v_has_account THEN
    UPDATE employee.account
    SET username = p_username,
      salt = v_salt,
      password_hash = v_hash
    WHERE employee_id = p_employee_id;
  ELSE
    INSERT INTO employee.account (
      employee_id,
      username,
      salt,
      password_hash
    ) VALUES (
      p_employee_id,
      p_username,
      v_salt,
      v_hash
    );
  END IF;
END;
$$ LANGUAGE plpgsql;
/

-- Create admin user
DO
$$DECLARE
  v_username VARCHAR := 'admin';
  v_password VARCHAR := &password;
BEGIN
  INSERT INTO employee.employee (employee_id, status, first_name, last_name, gender, date_of_birth, title, create_datetime, create_employee)
  VALUES (0, 'ADMIN', '', '', 'O', now(), '', now(), -1);

  SELECT create_account(0, v_username, v_password, -1);
END$$;
/

-- Authenticate user
SELECT authenticate_user(&username, &password);
/