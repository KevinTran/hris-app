create or replace view employee.v_amount_spent_per_month as
select
  month,
  year,
  sum(amount_paid)
from (
  select
    to_char(tv.timesheet_date, 'MM') as month,
    to_char(tv.timesheet_date, 'YYYY') as year,
    t.amount_paid
  from employee.timesheet_value tv
    join (
      select
        t.timesheet_id,
        sum(t.amount_paid) as amount_paid
      from employee.timesheet t
      group by t.timesheet_id
    ) t
      on t.timesheet_id = tv.timesheet_id
  group by
    to_char(tv.timesheet_date, 'MM'),
    to_char(tv.timesheet_date, 'YYYY'),
    t.amount_paid
) a
group by
  month,
  year;

grant select on employee.v_amount_spent_per_month to "appUser";