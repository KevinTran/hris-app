// Import environment variables
require('dotenv').config({ path: './process.env' });
console.log('Initialized environment variables in process.env');

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const fs = require('fs');
const compressor = require('node-minify');
const compression = require('compression');

const app = express();
app.use(compression());

// Jade view engine
app.set('views', path.join(__dirname, 'app/client/views'));
app.set('view engine', 'jade');

// Sequelize
let db = require('./app/server/models/index');

// Minify JS
compressor.minify({
  compressor: 'butternut',
  input: ['./app/client/*.js', './app/client/controllers/**/*.js'],
  output: './build/app.min.js'
});

// Express
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'app/server/public'),
  dest: path.join(__dirname, 'app/server/public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true,
  outputStyle: 'compressed'
}));
app.use(express.static(path.join(__dirname, 'app/server/public')));
app.use('/vendors', express.static(path.join(__dirname, 'app/client/vendors')));
app.use('/node_modules', express.static(path.join(__dirname, 'node_modules')));
app.use('/app.min.js', express.static(path.join(__dirname, 'build/app.min.js')));

// Routers
require('./app/server/routes/index')(app);

// 404 handler
app.use((req, res, next) => next(createError(404)));

// Error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
