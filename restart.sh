#!/bin/bash

PIDS=$(ps aux | grep '[-]Dport=3000' | awk '{print $2}')
if [ PIDS ]; then
  sudo kill $PIDS
  echo "Application stopped on port 3000"
fi

sudo npm start > log/app.log 2>&1 &
echo "Application started on port 3000"