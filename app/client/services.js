angular.module('hris')
  .service('employeeService', ['$http', '$rootScope', function ($http, $rootScope) {
    this.deserializeEmployeeData = (employee) => {
      employee.dateOfBirth = moment(employee.dateOfBirth).toDate();
      employee.hireDatetime = employee.hireDatetime ? moment(employee.hireDatetime).toDate() : null;
      employee.phoneNumber = +employee.phoneNumber;
      employee.payRate = employee.payRate ? Number.parseFloat(employee.payRate) : 0;
      return employee;
    };

    this.serializeEmployeeData = (employee) => {
      if (employee.gender) {
        employee.gender = employee.gender.charAt(0);
      }
      return employee;
    };

    this.setEmployees = (employees) => {
      this.employeeMap = {};
      employees.forEach(employee => this.employeeMap[employee.employeeId] = this.deserializeEmployeeData(employee));
      this.employees = employees;
    };
    this.addEmployee = (employee) => {
      this.employeeMap[employee.employeeId] = this.deserializeEmployeeData(employee)
      this.employees.push(employee);
    };
    this.getEmployees = () => this.employees;
    this.getEmployee = (employeeId) => this.employeeMap && this.employeeMap[employeeId];
    this.getCurrentEmployee = () => this.employeeMap && this.employeeMap[this.currentEmployeeId];
    this.setCurrentEmployee = (employeeId) => this.currentEmployeeId = employeeId;
    this.loadEmployees = () => {
      return new Promise((resolve, reject) => {
        $http({ url: '/app/api/employees', method: 'GET' })
          .then(
            results => {
              this.setEmployees(results.data.data);
              $rootScope.$emit('currentEmployeeSet', this);
              $rootScope.$emit('employeesLoaded', this);
              resolve(this.getEmployees());
            },
            err => reject(err)
          );
      });
    };

    this.createEmployeeRecord = async (employee) => {
      return new Promise((resolve, reject) => {
        $http({
          url: '/app/api/employee',
          method: 'POST',
          data: {
            status: employee.status,
            firstName: employee.firstName,
            lastName: employee.lastName,
            gender: employee.gender,
            dateOfBirth: employee.dateOfBirth,
            title: employee.title,
            payRate: employee.payRate,
            managerId: employee.managerId,
            phoneNumber: employee.phoneNumber,
            hireDatetime: employee.hireDatetime,
            inactivateDatetime: employee.inactivateDatetime,
            reasonForInactivation: employee.reasonForInactivation,
            createDatetime: new Date(),
            createEmployee: 0
          }
        })
          .then(results => resolve(results.data.data))
          .catch(error => reject(error));
      });
    };

    this.createEmployeeAddressRecord = async (employeeId, employee) => {
      return new Promise((resolve, reject) => {
        $http({
          url: '/app/api/employee/address/' + employeeId,
          method: 'PUT',
          data: {
            employeeId: employeeId,
            address: employee.employeeAddress.address,
            city: employee.employeeAddress.city,
            state: employee.employeeAddress.state,
            zipcode: employee.employeeAddress.zipcode,
            title: employee.employeeAddress.title,
            createDatetime: new Date(),
            createEmployee: 0
          }
        })
          .then(results => resolve(results.data.data))
          .catch(error => reject(error));
      });
    };

    this.createOrUpdateAccountRecord = async (employeeId, account) => {
      return new Promise((resolve, reject) => {
        $http({
          url: '/app/api/update-account/' + employeeId,
          method: 'PUT',
          headers: { Key: window.btoa(unescape(encodeURIComponent(account.username + ':' + account.password))) },
          params: {
            createEmployeeId: this.getCurrentEmployee().employeeId
          }
        })
          .then(results => resolve(results.data.data))
          .catch(error => reject(error));
      });
    };

    this.updateEmployee = async (employee, account) => {
      employee = this.serializeEmployeeData(employee);
      try {
        let savedEmployee = await this.updateEmployeeRecord(employee);
        if (savedEmployee) {
          if (employee.employeeAddress) {
            await this.updateEmployeeAddress(savedEmployee.employeeId, employee);
          }
          if (account) {
            await this.createOrUpdateAccountRecord(savedEmployee.employeeId, account);
          }
        }
      } catch (error) {
        window.alert(error);
      }
    };

    this.createEmployee = (employee, account) => {
      return new Promise(async (resolve, reject) => {
        employee = this.serializeEmployeeData(employee);
        try {
          let savedEmployee = await this.createEmployeeRecord(employee);
          if (savedEmployee) {
            if (employee.employeeAddress) {
              let employeeAddress = await this.createEmployeeAddressRecord(savedEmployee.employeeId, employee);
              savedEmployee.employeeAddress = employeeAddress;
            }
            if (account) {
              let savedAccount = await this.createOrUpdateAccountRecord(savedEmployee.employeeId, account);
              savedEmployee.account = savedAccount;
            }

            if (!savedEmployee.forms || savedEmployee.forms.length === 0) {
              let forms = await this.createForms(savedEmployee.employeeId, [{
                name: 'EMPLOYMENT_HISTORY',
                display: 'Employment History'
              }]);
              // $scope.employee.forms.push(form);
              // $scope.forms.push(form);
              savedEmployee.forms = forms;
            }
          }
          resolve(savedEmployee);
        } catch (error) {
          reject(error);
        }
      });
    };

    this.createForms = async (employeeId, formTypes) => {
      return new Promise((resolve, reject) => {
        let forms = [];
        let count = 0;
        formTypes.forEach(formType => {
          $http({
            url: '/app/api/form',
            method: 'POST',
            data: {
              employeeId: employeeId,
              formName: formType.name,
              formDisplay: formType.display,
              status: 'ACTIVE',
              createDatetime: new Date(),
              createEmployee: this.getCurrentEmployee().employeeId
            }
          }).then(
            results => {
              let form = results.data.data;
              forms.push(form);
              count++;
              if (count === formTypes.length) {
                resolve(forms);
              }
            },
            error => {
              count++;
              if (count === formTypes.length) {
                resolve(forms);
              }
            }
          );
        });
      });
    };

    this.updateForm = async (form) => {
      return new Promise((resolve, reject) => {
        form.updateDatetime = new Date();
        form.updateEmployee = this.getCurrentEmployee().employeeId;
        $http({
          url: '/app/api/form/' + form.formId,
          method: 'PUT',
          data: form
        })
          .then(results => resolve(results.data.data))
          .catch(error => reject(error));
      });
    };

    this.updateTimesheet = async (timesheet) => {
      return new Promise((resolve, reject) => {
        timesheet.updateDatetime = new Date();
        timesheet.updateEmployee = this.getCurrentEmployee().employeeId;
        $http({
          url: '/app/api/timesheet/' + timesheet.timesheetId,
          method: 'PUT',
          data: timesheet
        })
          .then(results => resolve(results.data.data))
          .catch(error => reject(error));
      });
    };

    this.updateEmployeeRecord = async (employee) => {
      return new Promise((resolve, reject) => {
        $http({
          url: '/app/api/employee/' + employee.employeeId,
          method: 'PUT',
          data: {
            firstName: employee.firstName,
            lastName: employee.lastName,
            gender: employee.gender,
            dateOfBirth: employee.dateOfBirth,
            title: employee.title,
            payRate: employee.payRate,
            managerId: employee.managerId,
            phoneNumber: employee.phoneNumber,
            hireDatetime: employee.hireDatetime,
            inactivateDatetime: employee.inactivateDatetime,
            reasonForInactivation: employee.reasonForInactivation,
            updateDatetime: new Date(),
            updateEmployee: 0
          }
        }).then(
          results => resolve(results.data.data),
          error => reject(error)
        );
      });
    };

    this.updateEmployeeStatus = async (employee) => {
      return new Promise((resolve, reject) => {
        $http({
          url: '/app/api/employee/' + employee.employeeId,
          method: 'PUT',
          data: {
            status: employee.status,
            updateDatetime: new Date(),
            updateEmployee: 0
          }
        }).then(
          results => resolve(results.data.data),
          error => reject(error)
        );
      });
    };

    this.updateEmployeeAddress = (employeeId, employee) => {
      return new Promise((resolve, reject) => {
        $http({
          url: '/app/api/employee/address/' + employeeId,
          method: 'PUT',
          data: {
            address: employee.employeeAddress.address,
            city: employee.employeeAddress.city,
            state: employee.employeeAddress.state,
            zipcode: employee.employeeAddress.zipcode
          }
        }).then(
          results => resolve(results.data.data),
          error => reject(error)
        );
      });
    };
  }]);