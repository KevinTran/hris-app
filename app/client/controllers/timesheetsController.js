angular.module('hris')
  .controller('timesheetsController', [
    '$scope',
    '$rootScope',
    '$http',
    ($scope, $rootScope, $http) => {
      $scope.view = 'timesheets';

      $scope.moment = moment;

      $scope.timesheetMap = {};
      $scope.timesheets = [];
      $scope.timesheetValueMap = {};
      $scope.timesheetValues = [];

      $scope.renderDate = value => moment(value, 'YYYY-MM-DD').format('M/D/YY');
      $scope.renderTime = value => value ? moment(value).format('h:mm A') : '-';
      $scope.renderHours = (value) => value ? (Math.floor(value).toString().padStart(2, 0) + ':' + Math.round((value * 60) % 60).toString().padStart(2, 0)) : '00:00';
      $scope.renderAmount = value => value ? '$' + (+value).toFixed(2) : '$0.00';

      // Load current employee timesheets
      $rootScope.$on('currentEmployeeSet', function (event, employeeService) {
        let employee = employeeService.getCurrentEmployee();
        $scope.employeeId = employee.employeeId;
        $scope.setTimesheets(employee.timesheets);
      });

      $rootScope.$on('timeClockUpdate', function (event, timesheet) {
        if (!$scope.timesheetMap[timesheet.timesheetId]) {
          $scope.timesheets.push(timesheet);
        }
        $scope.timesheetMap[timesheet.timesheetId] = timesheet;
      });

      $scope.setTimesheets = (timesheets) => {
        $scope.timesheetMap = {};
        $scope.timesheets = [];
        timesheets.forEach(timesheet => {
          if (!$scope.timesheetMap[timesheet.timesheetId]) {
            timesheet.startDateDisplay = moment(timesheet.startDate, 'YYYY-MM-DD').format('M/D/YY');
            timesheet.endDateDisplay = moment(timesheet.endDate, 'YYYY-MM-DD').format('M/D/YY');
            $scope.timesheetMap[timesheet.timesheetId] = timesheet;
            $scope.timesheets.push(timesheet);
          }
        });
      };

      $scope.setTimesheetValues = (timesheet) => {
        $scope.timesheetValueMap = {};
        $scope.timesheetValues = [];
        $scope.timesheetId = timesheet.timesheetId;
        $scope.employeeId = timesheet.employeeId;
        $scope.title =
          moment(timesheet.startDate).format('MM/DD/YYYY')
          + ' - '
          + moment(timesheet.endDate).format('MM/DD/YYYY');
        timesheet.timesheetValues.forEach(timesheetValue => {
          if (!$scope.timesheetValueMap[timesheetValue.timesheetValueId]) {
            $scope.timesheetValueMap[timesheetValue.timesheetValueId] = timesheetValue;
            $scope.timesheetValues.push(timesheetValue);
          }
        });
      };

      $scope.onTimesheetClick = (event) => {
        let row = event.currentTarget;
        let timesheetId = row.getAttribute('data-timesheetId');
        let timesheet = $scope.timesheetMap[timesheetId];
        $scope.setTimesheetValues(timesheet);
        $scope.view = 'timesheetValues';
      };

      $scope.onBackBtnClick = () => {
        $scope.view = 'timesheets';
      };

      $scope.clockIn = () => {
        $http({
          url: '/app/api/timesheet/' + $scope.employeeId + '/clockIn',
          method: 'GET'
        }).then(
          () => $scope.refreshTimesheetValues(),
          error => console.error(error)
        );
      };

      $scope.clockOut = () => {
        $http({
          url: '/app/api/timesheet/' + $scope.employeeId + '/clockOut',
          method: 'GET'
        }).then(
          () => $scope.refreshTimesheetValues(),
          error => console.error(error)
        );
      };

      $scope.refreshTimesheetValues = () => {
        $http({
          url: '/app/api/timesheet/' + $scope.timesheetId,
          method: 'GET'
        }).then(
          results => {
            let timesheet = results.data.data;
            if ($scope.view === 'timesheetValues') {
              $scope.setTimesheetValues();
            } else if ($scope.view === 'timesheets') {
              if (!$scope.timesheetMap[timesheet.timesheetId]) {
                $scope.timesheets.push(timesheet);
              }
              $scope.timesheetMap[timesheet.timesheetId] = timesheet;
            }
          },
          error => console.error(error)
        );
      };
    }]);