angular.module('hris')
  .controller('timeClockController', [
    '$scope',
    '$rootScope',
    '$http',
    ($scope, $rootScope, $http) => {
      $scope.date = moment().format('MMMM Do YYYY');

      $scope.formatTime = (value) => value ? moment(value).format('h:mm A') : null;
      $scope.formatHours = (value) => value ? (Math.floor(value).toString().padStart(2, 0) + ':' + Math.round((value * 60) % 60).toString().padStart(2, 0)) : '00:00';

      $rootScope.$on('currentEmployeeSet', (event, employeeService) => {
        let employee = employeeService.getCurrentEmployee();
        $scope.employeeId = employee.employeeId;
        $scope.loadCurrentEmployeeTimesheetValues();
      });

      $scope.loadCurrentEmployeeTimesheetValues = () => {
        $http({
          url: '/app/api/current-timesheet/' + $scope.employeeId,
          method: 'GET',
          params: {
            timesheetDate: moment().startOf('day').format('MM/DD/YYYY')
          }
        }).then(
          result => {
            let timesheet = result.data.data;
            if (timesheet) {
              $scope.timesheetValues = timesheet.timesheetValues;
              $rootScope.$emit('timeClockUpdate', timesheet);
            }
          },
          error => window.alert(error)
        );
      };

      $scope.clockIn = () => {
        $http({
          url: '/app/api/timesheet/' + $scope.employeeId + '/clockIn',
          method: 'GET',
          params: {
            timesheetDate: moment().startOf('day').format('MM/DD/YYYY')
          }
        }).then(
          () => $scope.loadCurrentEmployeeTimesheetValues(),
          error => console.error(error)
        );
      };

      $scope.clockOut = () => {
        $http({
          url: '/app/api/timesheet/' + $scope.employeeId + '/clockOut',
          method: 'GET',
          params: {
            timesheetDate: moment().startOf('day').format('MM/DD/YYYY')
          }
        }).then(
          () => $scope.loadCurrentEmployeeTimesheetValues(),
          error => console.error(error)
        );
      };
    }]);