angular.module('hris')
  .controller('formsController', [
    '$scope',
    '$rootScope',
    'employeeService',
    ($scope, $rootScope, employeeService) => {

      $scope.view = 'grid';

      $scope.formatDate = value => moment(value).format('M/D/YYYY, h:mm A');

      $rootScope.$on('currentEmployeeSet', (event, employeeService) => {
        let employee = employeeService.getCurrentEmployee();
        $scope.formMap = {};
        employee.forms.forEach(form => $scope.formMap[form.formId] = form);
        $scope.forms = employee.forms;
      });

      $scope.viewForm = (event) => {
        let btn = event.currentTarget;
        let formId = btn.getAttribute('data-formId');
        let form = $scope.formMap[formId];

        if (form) {
          $scope.form = form;
          $scope.switchView('form');
        }
      };

      $scope.onFormCancelBtnClick = () => {
        $scope.form = null;
        $scope.switchView('grid');
      };

      $scope.onFormSubmitBtnClick = async () => {
        $scope.form.status = 'SUBMITTED';
        await employeeService.updateForm($scope.form);
        $scope.switchView('grid');
        $scope.$apply();
      };

      // ----------------------------------------------------------------------
      // -- Handles view switching
      // ----------------------------------------------------------------------
      $scope.switchView = (view) => {
        $scope.view = view;
      };

    }]);