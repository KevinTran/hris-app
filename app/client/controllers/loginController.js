angular.module('hris')
  .controller('loginController', [
    '$scope',
    '$window',
    '$rootScope',
    '$http',
    '$cookies',
    'employeeService',
    ($scope, $window, $rootScope, $http, $cookies, employeeService) => {

      // Check for active session on startup
      let onSessionCheckSuccess = (results) => {
        $rootScope.existingSessionChecked = true;
        if (results) {
          // $rootScope.loggedInEmployee = results.data;
          $rootScope.loggedIn = true;
          employeeService.setCurrentEmployee(results.data.data.employeeId);
          $rootScope.$emit('loggedIn', employeeService);
        } else {
          $scope.logout();
        }
      };
      let onSessionCheckFailure = (result) => $rootScope.loggedIn = false;
      $http({
        url: '/app/api/check-session',
        method: 'GET',
        params: { _id: (new Date()).getTime() }
      }).then(onSessionCheckSuccess, onSessionCheckFailure);

      // Login form submission handler
      $scope.onLogin = () => {
        // Check for missing inputs
        if (!$scope.username || !$scope.password) {
          $rootScope.loginError = 'Please fill in all fields';
          return;
        }

        // Submit login request
        let onAuthenticationSuccess = (results) => {
          if (results) {
            $rootScope.loggedIn = true;
            $scope.username = null;
            $scope.password = null;
            employeeService.setCurrentEmployee(results.data.data.employeeId);
            $rootScope.$emit('loggedIn', employeeService);
          } else {
            $rootScope.loginError = 'Access denied';
            $scope.logout();
          }
          $scope.loggingIn = false;
        };
        let onAuthenticationFailure = (results) => {
          $rootScope.loginError = 'Access denied';
          $scope.logout();
          $scope.loggingIn = false;
        };

        $scope.loggingIn = true;

        $http({
          url: "/app/authenticate",
          method: "POST",
          headers: { Authorization: 'Basic ' + window.btoa(unescape(encodeURIComponent($scope.username + ':' + $scope.password))) }
        }).then(onAuthenticationSuccess, onAuthenticationFailure);
      };

      // Reset login form and fire logout handler
      $scope.onLogout = () => {
        $rootScope.loginError = null;
        $scope.logout();
      };

      // Switch to login screen and clear session token
      $scope.logout = () => {
        $rootScope.loggedIn = false;
        $http({
          url: '/app/logout',
          method: 'GET',
          params: { _id: (new Date()).getTime() }
        });
      };
    }]);