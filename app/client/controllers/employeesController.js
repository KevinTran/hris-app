angular.module('hris')
  .controller('employeesController', [
    '$scope',
    '$rootScope',
    '$http',
    'employeeService',
    ($scope, $rootScope, $http, employeeService) => {

      $scope.view = 'grid';
      $scope.employee = null;
      $scope.account = null;
      $scope.showAccountFields = false;
      $scope.submitBtnText = 'Submit';

      $scope.formatDate = value => moment(value).format('M/D/YYYY');
      $scope.renderDate = value => moment(value, 'YYYY-MM-DD').format('M/D/YY');
      $scope.renderTime = value => value ? moment(value).format('h:mm A') : '-';
      $scope.renderHours = (value) => value ? (Math.floor(value).toString().padStart(2, 0) + ':' + Math.round((value * 60) % 60).toString().padStart(2, 0)) : '00:00';
      $scope.renderAmount = value => value ? '$' + (+value).toFixed(2) : '$0.00';

      $rootScope.$on('loggedIn', (event, employeeService) => {
        employeeService.loadEmployees()
          .then(employees => {
            $scope.employees = employees;
            $scope.$apply();
          })
          .catch(err => console.log(err));
      });

      $scope.switchView = (view) => {
        $scope.view = view;
      };

      // ----------------------------------------------------------------------
      // -- Employee Grid
      // ----------------------------------------------------------------------
      $scope.viewProfile = (event) => {
        let btn = event.currentTarget;
        let employeeId = btn.getAttribute('data-employeeId');
        let employee = employeeService.getEmployee(employeeId);

        $scope.isUniqueUsername = false;
        $scope.isValidPassword = false;
        $scope.isMinLength = false;
        $scope.hasLowercase = false;
        $scope.hasUppercase = false;
        $scope.hasDigit = false;
        $scope.hasSymbol = false;
        $scope.passwordMatch = false;

        $scope.setTimesheets = (timesheets) => {
          $scope.timesheetMap = {};
          $scope.timesheets = [];
          timesheets.forEach(timesheet => {
            if (!$scope.timesheetMap[timesheet.timesheetId]) {
              $scope.timesheetMap[timesheet.timesheetId] = timesheet;
              $scope.timesheets.push(timesheet);
            }
          });
        };

        if (employee) {
          // Profile
          $scope.employee = Object.assign({}, employee);
          $scope.showAccountFields =
            $scope.employee.status === 'TEMPORARY'
            && !$scope.employee.account;
          $scope.account = null;
          $scope.title = 'Profile for '
            + $scope.employee.firstName
            + ' '
            + $scope.employee.lastName
            + ' (#'
            + $scope.employee.employeeId
            + ')';

          // Forms
          $scope.formMap = {};
          employee.forms.forEach(form => $scope.formMap[form.formId] = form);
          $scope.forms = employee.forms;
          $scope.formView = 'grid';

          // Timesheets
          $scope.employeeId = employee.employeeId;
          $scope.setTimesheets(employee.timesheets);
          $scope.timesheetView = 'timesheets';

          $scope.switchView('profile');
        }
      };

      $scope.onNewEmployeeBtnClick = () => {
        $scope.employee = {
          status: 'TEMPORARY'
        };
        $scope.showAccountFields = true;
        $scope.title = 'New Employee Profile';
        $scope.switchView('profile');
      };

      $scope.onBackBtnClick = () => {
        $scope.employee = null;
        $scope.account = null;
        $scope.switchView('grid');
      };

      // ----------------------------------------------------------------------
      // -- Profile Tab
      // ----------------------------------------------------------------------
      $scope.profile = {};

      $scope.profile.checkUsername = () => {
        if (!$scope.account.username) {
          $scope.isUniqueUsername = false;
          return;
        }

        if ($scope.account.username.length < 5) {
          $scope.isUniqueUsername = false;
          return;
        }

        $http({
          url: '/app/api/check-username',
          method: 'GET',
          params: {
            employeeId: $scope.employee.employeeId,
            customUsername: $scope.account.username
          }
        })
          .then(
            result => $scope.isUniqueUsername = result.data.isValid,
            error => $scope.isUniqueUsername = false
          );
      };

      $scope.profile.onPasswordChange = () => {
        let password = $scope.account.password;
        $scope.isMinLength = password.length >= 8;
        $scope.hasLowercase = /[a-z]/g.test(password);
        $scope.hasUppercase = /[A-Z]/g.test(password);
        $scope.hasDigit = /[0-9]/g.test(password);
        $scope.hasSymbol = /[!@#$%^&*()_+=-]/g.test(password);

        $scope.isValidPassword =
          $scope.isMinLength
          && $scope.hasLowercase
          && $scope.hasUppercase
          && $scope.hasDigit
          && $scope.hasSymbol;

        $scope.passwordMatch =
          $scope.isValidPassword
          && password === $scope.account.passwordRepeat;
      };

      $scope.profile.onSaveBtnClick = async () => {
        if (!$scope.employee.firstName
          || !$scope.employee.lastName
          || !$scope.employee.gender
          || !$scope.employee.dateOfBirth) {
          window.alert('Missing general information');
          return;
        }

        if (!$scope.employee.title
          || !$scope.employee.payRate) {
          window.alert('Invalid employment information');
          return;
        }

        let account;
        if ($scope.showAccountFields) {
          if (!$scope.isValidPassword
            || !$scope.passwordMatch
            || !$scope.isUniqueUsername) {
            window.alert('Invalid account information');
            return;
          }
          account = $scope.account;
        }

        if ($scope.employee.employeeId) {
          employeeService.updateEmployee($scope.employee, account);
        } else {
          let employee = await employeeService.createEmployee($scope.employee, account);
          employeeService.addEmployee(employee);
        }

        $scope.switchView('grid');
        $scope.$apply();
      };

      $scope.profile.getStatusColorCls = () => {
        if (!$scope.employee) {
          return '';
        }

        switch ($scope.employee.status) {
          case 'ACTIVE':
            return 'text-success';
          case 'TEMPORARY':
            return 'text-warning';
          case 'TERMINATED':
            return 'text-danger';
        }
      };

      $scope.onSetActiveStatus = () => {
        let employee = employeeService.getEmployee($scope.employee.employeeId);
        $scope.employee.status = 'ACTIVE';
        employee = Object.assign(employee, $scope.employee);
        employeeService.updateEmployeeStatus(employee, null);
        $('#employeeStatusModal').modal('hide');
      };

      $scope.onSetTerminatedStatus = () => {
        let employee = employeeService.getEmployee($scope.employee.employeeId);
        $scope.employee.status = 'TERMINATED';
        employee = Object.assign(employee, $scope.employee);
        employeeService.updateEmployeeStatus(employee, null);
        $('#employeeStatusModal').modal('hide');
      };

      // ----------------------------------------------------------------------
      // -- Forms Tab
      // ----------------------------------------------------------------------
      $scope.viewForm = (event) => {
        let btn = event.currentTarget;
        let formId = btn.getAttribute('data-formId');
        let form = $scope.formMap[formId];

        if (form) {
          $scope.form = form;
          $scope.formView = 'form';
        }
      };

      $scope.onFormSubmitBtnClick = async () => {
        $scope.form.status = 'SUBMITTED';
        await employeeService.updateForm($scope.form);
        $scope.formView = 'grid';
        $scope.$apply();
      };

      $scope.onFormSubmitCancelBtnClick = () => {
        $scope.submitBtnText = 'Submit'
        $scope.confirmSubmit = false;
      };

      $scope.onFormCancelBtnClick = () => {
        $scope.form = null;
        $scope.formView = 'grid';
      };

      $scope.addForm = (event) => {
        let btn = event.currentTarget;
        let formName = btn.getAttribute('data-formName');
        let formDisplay = btn.getAttribute('data-formDisplay');

        $http({
          url: '/app/api/form',
          method: 'POST',
          data: {
            employeeId: $scope.employee.employeeId,
            formName: formName,
            formDisplay: formDisplay,
            status: 'ACTIVE',
            createDatetime: new Date(),
            createEmployee: employeeService.getCurrentEmployee().employeeId
          }
        }).then(
          results => {
            let form = results.data.data;
            $scope.employee.forms.push(form);
            $scope.forms.push(form);
          },
          error => window.alert(error)
        );
      };

      // ----------------------------------------------------------------------
      // -- Timesheets Tab
      // ----------------------------------------------------------------------
      $scope.setTimesheetValues = (timesheet) => {
        $scope.timesheetValueMap = {};
        $scope.timesheetValues = [];
        $scope.timesheetId = timesheet.timesheetId;
        $scope.timesheet = timesheet;
        timesheet.timesheetValues.forEach(timesheetValue => {
          if (!$scope.timesheetValueMap[timesheetValue.timesheetValueId]) {
            $scope.timesheetValueMap[timesheetValue.timesheetValueId] = timesheetValue;
            $scope.timesheetValues.push(timesheetValue);
          }
        });
      };

      $scope.onTimesheetClick = (event) => {
        let row = event.currentTarget;
        let timesheetId = row.getAttribute('data-timesheetId');
        let timesheet = $scope.timesheetMap[timesheetId];
        $scope.setTimesheetValues(timesheet);
        $scope.timesheetView = 'timesheetValues';
      };

      $scope.onTimesheetBackBtnClick = () => {
        $scope.timesheetView = 'timesheets';
      };

      $scope.onPayTimesheet = async () => {
        $scope.timesheet.status = 'PAID';
        await employeeService.updateTimesheet($scope.timesheet);
        $scope.timesheetView = 'timesheets';
        $scope.$apply();
      };
    }]);