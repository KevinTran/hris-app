angular.module('hris')
  .controller('navController', [
    '$scope',
    '$rootScope',
    ($scope, $rootScope) => {
      $rootScope.$on('loggedIn', (event, employeeService) => {
        document.getElementById('nav-profile-tab').click();
      });

      $rootScope.$on('currentEmployeeSet', (event, employeeService) => {
        let employee = employeeService.getCurrentEmployee();
        $scope.employee = employee;
      });
    }]);