angular.module('hris')
  .controller('reportsController', [
    '$scope',
    '$http',
    ($scope, $http) => {
      $scope.data = [];

      $scope.startDate = moment().startOf('year').toDate();
      $scope.endDate = moment().endOf('year').toDate();

      $scope.runReport = () => {
        $http({
          url: '/app/api/report',
          method: 'GET',
          params: {
            startDate: moment($scope.startDate).format('MM-YYYY'),
            endDate: moment($scope.endDate).format('MM-YYYY')
          }
        })
          .then(results => {
            $scope.data = results.data.data[0];
            $scope.renderChart();
          })
          .catch(error => window.alert(error));
      };

      $scope.renderChart = () => {
        let revenueChart = new FusionCharts({
          "type": "column2d",
          "renderAt": "chartContainer",
          "width": "100%",
          "height": "400",
          "dataFormat": "json",
          "dataSource": {
            "chart": {
              "caption": "Monthly Spend",
              "subCaption": "",
              "xAxisName": "Month",
              "yAxisName": "Spend (In USD)",
              "theme": "fint"
            },
            "data": $scope.data
          }
        });

        revenueChart.render();
      }

      FusionCharts.ready($scope.renderChart);
    }]);