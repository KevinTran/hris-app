angular.module('hris')
  .controller('profileController', [
    '$scope',
    '$rootScope',
    'employeeService',
    ($scope, $rootScope, employeeService) => {
      $rootScope.$on('employeesLoaded', (event, employeeService) => {
        $scope.employee = employeeService.getCurrentEmployee();
        $scope.employees = employeeService.employees;
      });

      $scope.onSaveBtnClick = () => {
        employeeService.updateEmployee($scope.employee);
      };
    }]);