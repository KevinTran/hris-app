angular.module('hris', ['ngCookies', 'ngSanitize'])
  .factory('authInterceptor', ['$rootScope', '$q', '$cookies', ($rootScope, $q, $cookies) => {
    return {
      request: (config) => {
        config.headers = config.headers || {};
        config.params = config.params || {};
        config.params.username = $cookies.get('username');
        if ($cookies.get('sessionToken') && !config.headers.Authorization) {
          config.headers.Authorization = 'Bearer ' + $cookies.get('sessionToken');
        }
        return config;
      },
      response: (response) => {
        return response || $q.when(response);
      },
      responseError: (rejection) => {
        $rootScope.loggedIn = false;
      }
    };
  }])
  .config(['$httpProvider', ($httpProvider) => {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('authInterceptor');
  }]);

$('.navbar-nav>li>a').on('click', () => $('.navbar-collapse').collapse('hide'));