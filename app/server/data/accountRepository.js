const sequelize = require('../models').sequelize;
const Op = sequelize.Op;

const account = require('../models').account;

let self = module.exports = {

  createOrUpdateAccount(employeeId, username, password, createEmployeeId, transaction) {
    return new Promise((resolve, reject) => {
      sequelize.query('SELECT create_account($1, $2, $3, $4)', {
        bind: [
          employeeId,
          username,
          password,
          createEmployeeId
        ],
        transaction: transaction
      })
      .then(async results => {
        let account = await self.getAccountByUsername(username, transaction);
        delete account.dataValues.salt;
        delete account.dataValues.passwordHash;
        resolve(account);
      })
      .catch(error => reject(error));
    });
  },

  getAccountByUsername(username, transaction) {
    return new Promise((resolve, reject) => {
      account.findOne({
        where: {
          username: username
        },
        transaction: transaction
      })
        .then(results => resolve(results))
        .catch(error => reject(error));
    });
  },

  checkForExistingUsername(employeeId, username) {
    return new Promise((resolve, reject) => {
      account.count({
        where: {
          username: username,
          employeeId: {
            [Op.ne]: employeeId
          }
        }
      })
        .then(result => resolve(result))
        .catch(error => reject(error));
    });
  },

  authenticateUser(username, password, transaction) {
    return new Promise((resolve, reject) => {
      sequelize.query('SELECT authenticate_user($1, $2)', {
        bind: [
          username,
          password
        ],
        transaction: transaction
      })
        .then(async results => {
          if (results[0][0].authenticate_user) {
            let account = await self.getAccountByUsername(username, transaction);
            delete account.dataValues.salt;
            delete account.dataValues.passwordHash;
            resolve(account);
          }
          reject();
        })
        .catch(error => reject(error));
    });
  }
};