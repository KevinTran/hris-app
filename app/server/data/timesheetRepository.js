const moment = require('moment');

const models = require('../models');
const timesheet = models.timesheet;
const timesheetValue = models.timesheetValue;

const Op = models.sequelize.Op;

let self = module.exports = {
  createTimesheet(data, transaction) {
    return new Promise((resolve, reject) => {
      timesheet.create(data, {
        transaction: transaction
      })
        .then(result => resolve(result))
        .catch(error => reject(error));
    });
  },

  updateTimesheet(timesheetId, data, transaction) {
    return new Promise((resolve, reject) => {
      timesheet.update(data, {
        where: {
          timesheetId: timesheetId,
          status: { [Op.ne]: 'INACTIVE' }
        },
        transaction: transaction
      })
        .then(result => resolve(result))
        .catch(error => reject(error));
    });
  },

  createTimesheetValue(data, transaction) {
    return new Promise((resolve, reject) => {
      timesheetValue.create(data, {
        transaction: transaction
      })
        .then(result => resolve(result))
        .catch(error => reject(error));
    });
  },

  updateTimesheetValue(employeeId, timesheetDate, action, transaction) {
    return new Promise(async (resolve, reject) => {
      let timesheet = await self.getTimesheetByDate(employeeId, timesheetDate);
      if (!timesheet) {
        timesheet = await self.createTimesheet({
          employeeId: employeeId,
          status: 'ACTIVE',
          startDate: moment().startOf('month').toDate(),
          endDate: moment().endOf('month').subtract(1, 'day').toDate(),
          createDatetime: new Date(),
          createEmployee: 0
        });
      }

      let timesheetValues = timesheet.getDataValue('timesheetValues');
      let openTimesheetValue;
      (timesheetValues || []).forEach(timesheetValue => {
        let valueDate = moment(timesheetValue.getDataValue('timesheetDate')).toDate();
        console.log('Value Date Comparison: ', valueDate.toISOString() === timesheetDate.toISOString());
        if (valueDate.toISOString() === timesheetDate.toISOString()
          && (!timesheetValue.getDataValue('timeIn') || !timesheetValue.getDataValue('timeOut'))) {
          openTimesheetValue = timesheetValue;
        }
      });

      if (action === 'clockIn') {
        if (!openTimesheetValue) {
          openTimesheetValue = await self.createTimesheetValue({
            timesheetId: timesheet.getDataValue('timesheetId'),
            timesheetDate: timesheetDate,
            timeIn: moment.utc().toISOString(),
            createDatetime: new Date(),
            createEmployee: 0
          });
        }
      } else if (action === 'clockOut') {
        if (openTimesheetValue && !openTimesheetValue.getDataValue('timeOut')) {
          openTimesheetValue.set({
            timeOut: moment.utc().toISOString(),
            updateDatetime: new Date(),
            updateEmployee: 0
          });
          await openTimesheetValue.save();
        }
      }

      resolve(timesheet);
    });
  },

  async getTimesheetByDate(employeeId, timesheetDate) {
    return new Promise((resolve, reject) => {
      timesheet.findOne({
        where: {
          startDate: { [Op.lte]: timesheetDate },
          endDate: { [Op.gte]: timesheetDate },
          status: 'ACTIVE',
          employeeId: employeeId
        },
        include: [{
          model: timesheetValue,
          required: false
        }]
      })
        .then(record => resolve(record))
        .catch(() => reject());
    });
  },

  getTimesheet(timesheetId) {
    return new Promise((resolve, reject) => {
      timesheet.findById(timesheetId, {
        include: [{
          model: timesheetValue,
          required: false
        }]
      })
        .then(records => resolve(records))
        .catch(error => reject(error));
    });
  },

  getCurrentTimesheetValuesByEmployee(employeeId, timesheetDate, transaction) {
    return new Promise((resolve, reject) => {
      timesheet.findOne({
        where: {
          employeeId: employeeId,
          status: 'ACTIVE'
        },
        include: [{
          model: timesheetValue,
          where: {
            timesheetDate: timesheetDate
          }
        }],
        transaction: transaction
      })
        .then(record => resolve(record))
        .catch(error => reject(error));
    });
  }
};