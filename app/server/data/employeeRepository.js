const models = require('../models');
const Op = models.sequelize.Op;

const employee = models.employee;
const employeeAddress = models.employeeAddress;
const timesheet = models.timesheet;
const timesheetValue = models.timesheetValue;
const account = models.account;
const form = models.form;

let self = module.exports = {
  createEmployee(data, transaction) {
    return new Promise((resolve, reject) => {
      employee.create(data, {
        transaction: transaction,
        returning: true
      })
        .then(result => {
          self.getEmployeeById(result.get('employeeId'), transaction)
            .then(record => resolve(record))
            .catch(error => reject(error));
        })
        .catch(error => reject(error));
    });
  },

  updateEmployee(employeeId, data, transaction) {
    return new Promise((resolve, reject) => {
      employee.update(data, {
        where: {
          employeeId: employeeId
        },
        transaction: transaction,
        returning: true
      })
        .then((results) => {
          self.getEmployeeById(results[1][0].get('employeeId'))
            .then(record => resolve(record))
            .catch(error => reject(error));
        })
        .catch(error => reject(error));
    });
  },

  getEmployeeById(employeeId, transaction) {
    return new Promise((resolve, reject) => {
      employee.findById(employeeId, {
        transaction: transaction,
        include: [{
          model: employeeAddress,
          required: false
        }, {
          model: timesheet,
          required: false,
          include: [{
            model: timesheetValue,
            required: false
          }]
        }, {
          model: account,
          attributes: ['employeeId', 'username'],
          required: false
        }, {
          model: form,
          required: false
        }]
      })
        .then(record => resolve(record))
        .catch(error => reject(error));
    });
  },

  findOrCreateEmployeeAddress(employeeId, data, transaction) {
    return new Promise((resolve, reject) => {
      data.createDatetime = new Date();
      data.createEmployee = 0;

      employeeAddress.findOrCreate({
        where: {
          employeeId: employeeId
        },
        defaults: data,
        transaction: transaction
      })
        .then((results) => {
          let model = results[0];
          let created = results[1];
          if (!created) {
            delete data.createDatetime;
            delete data.createEmployee;
            data.updateDatetime = new Date();
            data.updateEmployee = 0;
            model.set(data);
            model.save()
              .then(record => resolve(record))
              .catch(error => reject(error));
          } else {
            resolve(model);
          }
        })
        .catch(error => reject(error));
    });
  },

  getEmployees(query) {
    return new Promise((resolve, reject) => {
      employee.findAll({
        where: query,
        order: [
          ['employeeId', 'ASC']
        ],
        where: {
          status: { [Op.ne]: 'ADMIN' }
        },
        include: [{
          model: employeeAddress,
          required: false
        }, {
          model: timesheet,
          required: false,
          include: [{
            model: timesheetValue,
            required: false
          }]
        }, {
          model: account,
          attributes: ['employeeId', 'username'],
          required: false
        }, {
          model: form,
          required: false
        }]
      })
        .then(result => resolve(result))
        .catch(error => reject(error));
    });
  }
};