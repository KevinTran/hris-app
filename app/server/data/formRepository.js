const models = require('../models');
const Op = models.sequelize.Op;

const form = models.form;

let self = module.exports = {
  getByEmployeeId(employeeId, transaction) {
    return new Promise((resolve, reject) => {
      form.findAll({
        where: {
          employeeId: employeeId,
          status: {
            [Op.in]: ['ACTIVE', 'COMPLETE']
          }
        },
        transaction: transaction
      })
        .then(records => resolve(records))
        .catch(error => reject(error));
    });
  },

  create(data, transaction) {
    return new Promise((resolve, reject) => {
      form.create(data, {
        transaction: transaction
      })
        .then(record => resolve(record))
        .catch(error => reject(error));
    });
  },

  update(formId, data, transaction) {
    return new Promise((resolve, reject) => {
      form.update(data, {
        where: {
          formId: formId
        },
        transaction: transaction,
        returning: true
      })
        .then(records => resolve(records))
        .catch(error => reject(error));
    });
  }
};