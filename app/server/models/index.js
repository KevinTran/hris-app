const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const basename = path.basename(module.filename);

// Import environment variables
let envConfigPath = path.join(__dirname, '../../../process.env');
require('dotenv').config({ path: envConfigPath });
console.log('Initialized environment variables in process.env using \'' + envConfigPath + '\'');

const sequelize = new Sequelize(process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,
  pool: {
    max: process.env.DB_POOL_MAX,
    min: process.env.DB_POOL_MIN,
    acquire: process.env.DB_POOL_ACQUIRE,
    idle: process.env.IDLE
  },
  operatorsAliases: false
});

sequelize
  .authenticate()
  .then(() => console.log('Connected to PostgreSQL DB.'))
  .catch((err) => console.error('[ERROR] Unable to connect to PostgreSQL DB. =>', err));

let db = {};

// Import models
console.log('Searching for Sequelize models...');
const walkSync = (d) => fs.statSync(d).isDirectory() ? fs.readdirSync(d).map(f => walkSync(path.join(d, f))) : d;
let files = walkSync(__dirname);
let filesOnly = [].concat.apply([], files);
filesOnly.filter((file) =>
  (file.indexOf('.') !== 0) &&
  (file !== basename) &&
  (file.slice(-3) === '.js') &&
  (file.indexOf('index.js') === -1))
  .forEach(file => {
    const model = sequelize.import(file);
    db[model.name] = model;
    console.log('- Initialized model for table \'' + model.options.schema + '.' + model.tableName + '\'');
  });
console.log('...Done.\n');

// Associations
db.employee.hasOne(db.employeeAddress, { foreignKey: 'employee_id' });
db.employee.hasOne(db.account, { foreignKey: 'employee_id' });
db.employee.hasMany(db.timesheet, { foreignKey: 'employee_id' });
db.employee.hasMany(db.form, { foreignKey: 'employee_id' });
db.timesheet.hasMany(db.timesheetValue, { foreignKey: 'timesheet_id' });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;