module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    'employeeAddress', {
      employeeId: {
        field: 'employee_id',
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      address: {
        field: 'address',
        type: DataTypes.STRING
      },
      city: {
        field: 'city',
        type: DataTypes.STRING
      },
      state: {
        field: 'state',
        type: DataTypes.STRING
      },
      zipcode: {
        field: 'zipcode',
        type: DataTypes.STRING
      },
      createDatetime: {
        field: 'create_datetime',
        type: DataTypes.DATE
      },
      createEmployee: {
        field: 'create_employee',
        type: DataTypes.INTEGER
      },
      updateDatetime: {
        field: 'update_datetime',
        type: DataTypes.DATE
      },
      updateEmployee: {
        field: 'update_employee',
        type: DataTypes.INTEGER
      }
    }, {
      schema: 'employee',
      tableName: 'employee_address',
      timestamps: false
    }
  );
};