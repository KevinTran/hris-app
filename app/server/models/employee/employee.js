module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    'employee', {
      employeeId: {
        field: 'employee_id',
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        defaultValue: null
      },
      status: {
        field: 'status',
        type: DataTypes.ENUM,
        values: ['ACTIVE', 'TEMPORARY', 'TERMINATED']
      },
      firstName: {
        field: 'first_name',
        type: DataTypes.STRING
      },
      lastName: {
        field: 'last_name',
        type: DataTypes.STRING
      },
      gender: {
        field: 'gender',
        type: DataTypes.CHAR,
        values: ['M', 'F']
      },
      dateOfBirth: {
        field: 'date_of_birth',
        type: DataTypes.DATEONLY
      },
      title: {
        field: 'title',
        type: DataTypes.STRING
      },
      payRate: {
        field: 'pay_rate',
        type: DataTypes.FLOAT
      },
      managerId: {
        field: 'manager_id',
        type: DataTypes.INTEGER
      },
      phoneNumber: {
        field: 'phone_number',
        type: DataTypes.STRING
      },
      hireDatetime: {
        field: 'hire_datetime',
        type: DataTypes.DATE
      },
      inactivateDatetime: {
        field: 'inactivate_datetime',
        type: DataTypes.DATE
      },
      inactivateEmployee: {
        field: 'create_employee',
        type: DataTypes.INTEGER
      },
      createDatetime: {
        field: 'create_datetime',
        type: DataTypes.DATE
      },
      createEmployee: {
        field: 'create_employee',
        type: DataTypes.INTEGER
      },
      updateDatetime: {
        field: 'update_datetime',
        type: DataTypes.DATE
      },
      updateEmployee: {
        field: 'update_employee',
        type: DataTypes.INTEGER
      }
    }, {
      schema: 'employee',
      tableName: 'employee',
      createdAt: false,
      updatedAt: false
    }
  );
};