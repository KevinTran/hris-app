module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    'account', {
      employeeId: {
        field: 'employee_id',
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      username: {
        field: 'username',
        type: DataTypes.STRING
      },
      salt: {
        field: 'salt',
        type: DataTypes.STRING
      },
      passwordHash: {
        field: 'password_hash',
        type: DataTypes.STRING
      }
    }, {
      schema: 'employee',
      tableName: 'account',
      createdAt: false,
      updatedAt: false
    }
  );
};