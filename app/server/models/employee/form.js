module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    'form', {
      formId: {
        field: 'form_id',
        type: DataTypes.BIGINT,
        primaryKey: true,
        autoIncrement: true,
        defaultValue: null
      },
      employeeId: {
        field: 'employee_id',
        type: DataTypes.INTEGER
      },
      formName: {
        field: 'form_name',
        type: DataTypes.STRING
      },
      formDisplay: {
        field: 'form_display',
        type: DataTypes.STRING
      },
      formData: {
        field: 'form_data',
        type: DataTypes.JSON
      },
      status: {
        field: 'status',
        type: DataTypes.STRING
      },
      createDatetime: {
        field: 'create_datetime',
        type: DataTypes.DATE
      },
      createEmployee: {
        field: 'create_employee',
        type: DataTypes.INTEGER
      },
      updateDatetime: {
        field: 'update_datetime',
        type: DataTypes.DATE
      },
      updateEmployee: {
        field: 'update_employee',
        type: DataTypes.INTEGER
      }
    }, {
      schema: 'employee',
      tableName: 'form',
      createdAt: false,
      updatedAt: false
    }
  );
};