module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    'timesheet', {
      timesheetId: {
        field: 'timesheet_id',
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        defaultValue: null
      },
      employeeId: {
        field: 'employee_id',
        type: DataTypes.INTEGER,
        allowNull: false
      },
      status: {
        field: 'status',
        type: DataTypes.STRING,
        allowNull: false
      },
      startDate: {
        field: 'start_date',
        type: DataTypes.DATE,
        allowNull: false
      },
      endDate: {
        field: 'end_date',
        type: DataTypes.DATE,
        allowNull: false
      },
      hours: {
        field: 'hours',
        type: DataTypes.FLOAT
      },
      amountPaid: {
        field: 'amount_paid',
        type: DataTypes.FLOAT
      },
      createDatetime: {
        field: 'create_datetime',
        type: DataTypes.DATE,
        allowNull: false
      },
      createEmployee: {
        field: 'create_employee',
        type: DataTypes.INTEGER,
        allowNull: false
      },
      updateDatetime: {
        field: 'update_datetime',
        type: DataTypes.DATE
      },
      updateEmployee: {
        field: 'update_employee',
        type: DataTypes.INTEGER
      }
    }, {
      schema: 'employee',
      tableName: 'timesheet',
      createdAt: false,
      updatedAt: false
    }
  );
};