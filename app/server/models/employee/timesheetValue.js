module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    'timesheetValue', {
      timesheetValueId: {
        field: 'timesheet_value_id',
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        defaultValue: null
      },
      timesheetId: {
        field: 'timesheet_id',
        type: DataTypes.INTEGER,
        allowNull: false
      },
      timesheetDate: {
        field: 'timesheet_date',
        type: DataTypes.DATE
      },
      timeIn: {
        field: 'time_in',
        type: DataTypes.TIME
      },
      timeOut: {
        field: 'time_out',
        type: DataTypes.TIME
      },
      hours: {
        field: 'hours',
        type: DataTypes.FLOAT
      },
      createDatetime: {
        field: 'create_datetime',
        type: DataTypes.DATE,
        allowNull: false
      },
      createEmployee: {
        field: 'create_employee',
        type: DataTypes.INTEGER,
        allowNull: false
      },
      updateDatetime: {
        field: 'update_datetime',
        type: DataTypes.DATE
      },
      updateEmployee: {
        field: 'update_employee',
        type: DataTypes.INTEGER
      }
    }, {
      schema: 'employee',
      tableName: 'timesheet_value',
      createdAt: false,
      updatedAt: false
    }
  );
};