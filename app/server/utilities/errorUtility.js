module.exports = {
  handleRequestError(error, status, req, res) {
    console.error('[ERROR] An error has occurred during a ' + req.method + ' request to ' + req.url + '!', error);
    res.status(status).send({ success: false, message: error });
  }
};