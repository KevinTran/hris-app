const expressJwt = require('express-jwt');
const jwt = require('jsonwebtoken');

const accountRepo = require('../data/accountRepository');

const secret = process.env.SESSION_TOKEN_SECRET;
const sessionTimeoutInMinutes = process.env.SESSION_TIMEOUT;

const salt = process.env.PASSWORD_SALT;

let self = module.exports = {
  verifySessionToken(authHeader) {
    if (!authHeader) {
      return false;
    }
    let tokens = authHeader.split(' ');
    return jwt.verify(tokens[1], secret);
  },

  renewSessionToken(username, res) {
    let token = jwt.sign({ data: username, date: new Date() }, secret, { expiresIn: sessionTimeoutInMinutes + 'm' });
    res.cookie('username', username, { path: '/app', expires: new Date(Date.now() + sessionTimeoutInMinutes * 60 * 1000) });
    res.cookie('sessionToken', token, { path: '/app', expires: new Date(Date.now() + sessionTimeoutInMinutes * 60 * 1000) });
  },

  clearSessionToken(res) {
    res.clearCookie('sessionToken', { path: '/app' });
    res.clearCookie('username', { path: '/app' });
  },

  decodeCredentials(str) {
    let result = Buffer.from(str.split(' ')[1], 'base64').toString('ascii');
    let tokens = result.split(':');
    return {
      username: tokens[0].toLowerCase().trim(),
      password: tokens[1]
    };
  },

  authenticateUser(username, password, transaction) {
    return new Promise((resolve, reject) => {
      accountRepo.authenticateUser(username, password, transaction)
        .then(result => resolve(result))
        .catch(error => reject(error));
    });
  },

  createOrUpdateAccount(employeeId, username, password, createEmployeeId, transaction) {
    return new Promise((resolve, reject) => {
      accountRepo.createOrUpdateAccount(employeeId, username, password, createEmployeeId, transaction)
        .then(results => resolve(results))
        .catch(error => reject(error));
    });
  },

  changePassword(employeeId, username, password, transaction) {
    return new Promise((resolve, reject) => {
      accountRepo.createOrUpdateAccount(employeeId, username, password, createEmployeeId, transaction)
        .then(results => resolve(results))
        .catch(error => reject(error));
    });
  },

  checkForExistingUsername(employeeId, username) {
    return new Promise((resolve, reject) => {
      accountRepo.checkForExistingUsername(employeeId, username)
        .then(result => resolve(result))
        .catch(error => reject(error));
    });
  }
};