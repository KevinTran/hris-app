const employeeRepo = require('../data/employeeRepository');

const errorUtility = require('../utilities/errorUtility');

module.exports = (app) => {
  app.post('/app/api/employee', (req, res) => {
    employeeRepo.createEmployee(req.body)
      .then(result => res.status(200).send({ success: true, data: result }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.put('/app/api/employee/:employeeId', (req, res) => {
    employeeRepo.updateEmployee(req.params.employeeId, req.body)
      .then(result => res.status(200).send({ success: true, data: result }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.put('/app/api/employee/address/:employeeId', (req, res) => {
    employeeRepo.findOrCreateEmployeeAddress(req.params.employeeId, req.body)
      .then(result => res.status(200).send({ success: true, data: result }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.get('/app/api/employees', (req, res) => {
    employeeRepo.getEmployees({})
      .then(records => res.status(200).send({ success: true, data: records }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.get('/app/api/onboarding/employees', (req, res) => {
    employeeRepo.getEmployees({ status: 'TEMPORARY' })
      .then(records => res.status(200).send({ success: true, data: records }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });
};