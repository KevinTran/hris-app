const fs = require('fs');

module.exports = (app) => {
  app.get('/', function (req, res, next) {
    res.redirect('/app');
  });

  app.get('/app', function (req, res, next) {
    res.render('index', {});
  });

  // Import controllers
  console.log('Searching for routes...');
  fs.readdirSync(__dirname)
    .forEach(file => {
      if (file !== 'index.js') {
        require(__dirname + '/' + file)(app);
        console.log('- Initialized controller \'' + file + '\'');
      }
    });
  console.log('...Done.\n');
};