const sequelize = require('../models').sequelize;

const errorUtility = require('../utilities/errorUtility');

module.exports = (app) => {
  app.get('/app/api/report', (req, res) => {
    sequelize.query(
      'select month || \'-\' || year as label, sum as value ' +
      'from employee.v_amount_spent_per_month ' +
      'where (month || \'-\' || year) >= ? and (month || \'-\' || year) <= ?', {
        replacements: [req.query.startDate, req.query.endDate]
      }
    )
      .then(results => res.status(200).send({ success: true, data: results }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res))
  });
};