const securityUtility = require('../utilities/securityUtility');
const errorUtility = require('../utilities/errorUtility');

const accountRepo = require('../data/accountRepository');

module.exports = (app) => {
  app.use('/app/api', (req, res, next) => {
    if (securityUtility.verifySessionToken(req.header('authorization'))) {
      securityUtility.renewSessionToken(req.query.username, res);
      req.renewed = true;
    } else {
      securityUtility.clearSessionToken(res);
    }
    next();
  });

  app.get('/app/api/check-session', (req, res) => {
    if (req.renewed) {
      let credentials = securityUtility.decodeCredentials(req.headers.authorization);
      accountRepo.getAccountByUsername(req.query.username)
        .then(result => res.status(200).send({ success: !!result, data: result && { employeeId: result.employeeId, username: result.username } }))
        .catch(error => errorUtility.handleRequestError(error, 401, req, res));
    } else {
      errorUtility.handleRequestError('Session timed out', 401, req, res);
    }
  });

  app.post('/app/authenticate', (req, res) => {
    let credentials = securityUtility.decodeCredentials(req.headers.authorization);
    securityUtility.authenticateUser(credentials.username, credentials.password)
      .then(result => {
        securityUtility.renewSessionToken(result.username, res);
        res.status(200).send({ success: true, data: result });
      })
      .catch(error => errorUtility.handleRequestError(error, 401, req, res));
  });

  app.put('/app/api/update-account/:employeeId', (req, res) => {
    let credentials = securityUtility.decodeCredentials('Basic ' + req.headers.key);
    securityUtility.createOrUpdateAccount(req.params.employeeId, credentials.username, credentials.password, req.query.createEmployeeId)
      .then(result => res.status(200).send({ success: true, data: result }))
      .catch(error => errorUtility.handleRequestError(error, 401, req, res));
  });

  app.get('/app/logout', (req, res) => {
    securityUtility.clearSessionToken(res);
    res.status(200).send({});
  });

  app.get('/app/api/check-username', (req, res) => {
    securityUtility.checkForExistingUsername(req.query.employeeId, req.query.customUsername)
      .then(result => {res.status(200).send({ success: true, isValid: result === 0 })})
      .catch(error => errorUtility.handleRequestError(error, 401, req, res));
  });
};