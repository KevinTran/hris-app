const moment = require('moment');

const timesheetRepo = require('../data/timesheetRepository');

const errorUtility = require('../utilities/errorUtility');

module.exports = (app) => {
  app.get('/app/api/timesheet/:employeeId/:action', (req, res) => {
    let timesheetDate = new Date(req.query.timesheetDate);
    console.log(timesheetDate);
    timesheetRepo.updateTimesheetValue(req.params.employeeId, timesheetDate, req.params.action)
      .then(result => res.status(200).send({ success: true, data: result }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.get('/app/api/timesheet/:timesheetId', (req, res) => {
    timesheetRepo.getTimesheet(req.params.timesheetId)
      .then(result => res.status(200).send({ success: true, data: result }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.get('/app/api/current-timesheet/:employeeId', (req, res) => {
    let timesheetDate = new Date(req.query.timesheetDate);
    timesheetRepo.getCurrentTimesheetValuesByEmployee(req.params.employeeId, timesheetDate)
      .then(result => res.status(200).send({ success: true, data: result }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.put('/app/api/timesheet/:timesheetId', (req, res) => {
    timesheetRepo.updateTimesheet(req.params.timesheetId, req.body)
      .then(records => res.status(200).send({ success: true, data: records }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });
};