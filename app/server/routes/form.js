const formRepo = require('../data/formRepository');

const errorUtility = require('../utilities/errorUtility');

module.exports = (app) => {
  app.get('/app/api/form/:employeeId', (req, res) => {
    formRepo.getByEmployeeId(req.params.employeeId)
      .then(records => res.status(200).send({ success: true, data: records }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.post('/app/api/form', (req, res) => {
    formRepo.create(req.body)
      .then(record => res.status(200).send({ success: true, data: record }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });

  app.put('/app/api/form/:formId', (req, res) => {
    formRepo.update(req.params.formId, req.body)
      .then(records => res.status(200).send({ success: true, data: records }))
      .catch(error => errorUtility.handleRequestError(error, 400, req, res));
  });
};